package ru.t1.aksenova.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    @NotNull
    Date getCreated();

    @NotNull
    void setCreated(@NotNull Date created);

}
