package ru.t1.aksenova.tm.api.endpoint;


public interface IConnectionProvider {

    String host = "";

    String port = "";

    String getHost();

    String getPort();

}
