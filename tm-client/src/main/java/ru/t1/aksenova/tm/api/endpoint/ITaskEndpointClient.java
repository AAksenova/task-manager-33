package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.dto.response.*;

public interface ITaskEndpointClient {

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListResponse taskList(@NotNull TaskListRequest request);

    @NotNull
    TaskListByProjectIdResponse taskListByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

}
