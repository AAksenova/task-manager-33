package ru.t1.aksenova.tm.api.service;


import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.endpoint.IEndpointClient;
import ru.t1.aksenova.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

}
