package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.dto.response.*;

public interface IUserEndpointClient {

    @NotNull
    UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request);

    @NotNull
    UserLockResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    UserRemoveResponse userRemove(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request);

}
