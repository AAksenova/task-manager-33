package ru.t1.aksenova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();

        @Nullable final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(index, name, description);
        getProjectEndpointClient().updateProjectByIndex(request);
    }

}
