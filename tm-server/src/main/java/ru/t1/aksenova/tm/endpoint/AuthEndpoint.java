package ru.t1.aksenova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.service.IAuthService;
import ru.t1.aksenova.tm.api.service.IServiceLocator;
import ru.t1.aksenova.tm.api.service.IUserService;
import ru.t1.aksenova.tm.dto.request.UserLoginRequest;
import ru.t1.aksenova.tm.dto.request.UserLogoutRequest;
import ru.t1.aksenova.tm.dto.request.UserViewProfileRequest;
import ru.t1.aksenova.tm.dto.response.*;
import ru.t1.aksenova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    private IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        getAuthService().login(login, password);
        return new UserLoginResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        getAuthService().logout();
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
